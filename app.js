var config = require('./config.js');
var express = require('express');
var app = express();
var router = express.Router();
// setup static contents route
router.use(express.static(__dirname + '/public'));

app.use(router);
app.listen(8400);
console.log("Server running http://localhost:"+config.app_port);
