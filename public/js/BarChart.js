/**
 * Dynamic bar chart will scale based on width, height and number of items in dataset
 * Copyright © 2015 Thomas Bentz
 * Email: digitalblur1@gmail.com
 */

function BarChart(viz_id, title, data_file){
    var _self = this;
    _self.padding = 30;

    _self.title = title;
    _self.viz_id = viz_id;
    _self.dataSource = data_file;

    this.init = function(){
        _self.w = d3.select(_self.viz_id).attr("width");
        _self.h = d3.select(_self.viz_id).attr("height");




        // load the data from external souce
//        http://192.168.8.238:8082/products/get_aggregate
        d3.json(_self.dataSource, function(error, json) {
            if(error){
                json = _self.getRandomData();
//                _self.createViz(random_data);
//                    return console.warn(error);
            }
            _self.createViz(json);
        });
        return this;
    }

    this.refresh = function(title){
        var random_data = _self.getRandomData();
        this.updateViz(random_data);
        _self.chartTitle
            .text(_self.title + ' - ' + title)
    }

    this.getRandom = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    this.getRandomData = function(){
        var data = new Array();
        for(var i=0;i<=9;i++){
            var rand = this.getRandom(100, 900);
            data[i] = {"text": "Tour " + i, "data": rand}
        }
        return data;
    }


    this.updateViz = function(dataset){
        // setup our y scale based on our svg canvas height
        _self.yScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d.data; })])
            .range([_self.h - _self.padding, 0 + _self.padding + 50]);

        var anim_delay = 100;
        this.rect
            .data(dataset)
            .transition()
            .duration(1000)
            .delay(function(d, i){
                return i*anim_delay;
            })
            .attr("y", function(d, i) {
                // scale our bar height
                return _self.yScale(d.data);
            })
            .attr("height", function(d, i) {
                return _self.h - _self.yScale(d.data) - _self.padding;
            })
            .attr('fill', function(d, i){

                var r = Math.round(_self.cScaleR(d['data']));
                var g = Math.round(_self.cScaleG(d['data']));
                var b = Math.round(_self.cScaleB(d['data']));
                return "rgb(" + r + "," + g + "," + b + ")";

//                var g = Math.round(_self.cScale(d['data']));
//                console.log('col', g);
//                return "rgb(70,70," + g + ")";
//                return "rgb(0, " + g + ", 0)";
                // return (i%2)?'blue':'red'
            })
            .text(function(d) {
                // display the coordinates as text
                return "$" + d.data;
            });


        this.barTitles
            .data(dataset)
            .transition()
            .duration(1000)
            .delay(function(d, i){
                return i*anim_delay;
            })
            .attr("y", function(d, i) {
                // scale our bar height
                return _self.yScale(d.data) - 3;
            })

        this.textData
            .data(dataset)
            .transition()
            .duration(1000)
            .delay(function(d, i){
                return i*anim_delay;
            })
            .attr("y", function(d, i) {
                // scale our bar height
                return _self.yScale(d.data) + 11;
            })
            .text(function(d) {
                // display the coordinates as text
                return "$" + d.data;
            })

    }



    this.createViz = function(dataset){
        // create a horizontal scale based on the number of items in the dataset
        _self.xScale = d3.scale.ordinal()
            .domain(d3.range(dataset.length))
            .rangeRoundBands([_self.padding, _self.w-_self.padding], 0.2, 0.1);

        // setup our y scale based on our svg canvas height
        _self.yScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d.data; })])
            .range([_self.h - _self.padding, 0 + _self.padding + 50]); // + 24 for title offset

        // setup our color scale based on data size
        _self.cScaleR = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d['data']; })])
            .range([0, 49]);

        _self.cScaleG = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d['data']; })])
            .range([0, 141]);

        _self.cScaleB = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d['data']; })])
            .range([0, 222]);

        _self.myid = "mybar";// "viz" + new Date().getMilliseconds();

        // setup the svg canvas
        _self.svg = d3.select(_self.viz_id + '_group')
            .append("svg")
            .attr("width", _self.w)
            .attr("height", _self.h)
            .attr("id", _self.myid);

        // get the position of the module location
        _self.svgx = d3.select(_self.viz_id).attr("x");
        _self.svgy = d3.select(_self.viz_id).attr("y");

        // set the position of the chart
        _self.svg.attr('x', _self.svgx);
        _self.svg.attr('y', _self.svgy);


        // create background
        _self.bg = _self.svg.append("rect")
            .attr("width", _self.w)
            .attr("height", _self.h)
            .attr("rx", 6) // rounded corners
            .attr("ry", 6) // rounded corners
            .attr("fill", "rgb(236,236,236)");

        // create a group element then rectangles from data
        _self.rect = _self.svg.append("g")
            .selectAll("rect")
            .data(dataset)
            .enter()
            .append("rect")
            .attr('i', function(d){return d.data;})
            .attr('x', function(d, i){
                // use our scale on the x axis to spread out the bars evenly
                return _self.xScale(i);
            }).attr("y", function(d, i) {
                // scale our bar height
                return _self.h - _self.padding;
//                return _self.yScale(d.data);
            })
            .attr('width', _self.xScale.rangeBand())
            .attr("height", function(d, i) {
//                console.log(i, _self.yScale(d.data));
                return 0;
                //_self.yScale(d.data);
//                return _self.h - _self.yScale(d.data) - _self.padding;
            })
            .attr('fill', function(d, i){
                var r = Math.round(_self.cScaleR(d['data']));
                var g = Math.round(_self.cScaleG(d['data']));
                var b = Math.round(_self.cScaleB(d['data']));
                return "rgb(" + r + "," + g + "," + b + ")";
//                return "rgb(0, " + g + ", 0)";
                // return (i%2)?'blue':'red'
            })
            .attr('stroke-width', 1)
            .attr('stroke', 'black')

        _self.rect
            .transition()
            .duration(1000)
            .attr("y", function(d, i) {
                // scale our bar height
                return _self.yScale(d.data) + _self.padding;
            })
            .attr("height", function(d, i) {
                return _self.h - _self.yScale(d.data) - _self.padding*2;
            })

        // create a group element, then text from data
        _self.barTitles = _self.svg.append("g").selectAll("text")
            .data(dataset)
            .enter()
            .append("text")
            .text(function(d) {
                // display the text
                return d.text;
            })
            .attr("x", function(d, i) {
                // center text with middle alignment
                return _self.xScale(i) + _self.xScale.rangeBand() / 2;
            })
            .attr("y", function(d) {
                // display text above the bar
                return _self.h - _self.padding;
            })
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("font-size", "11px")
            .attr("fill", "black");

        _self.barTitles
            .transition()
            .duration(1000)
            .attr("y", function(d, i) {
                // scale our bar height
                return _self.yScale(d.data)  + _self.padding - 3;
            })


        // create a group element, then create the data as text
        _self.textData = _self.svg.append("g").selectAll("text")
            .data(dataset)
            .enter()
            .append("text")
            .text(function(d) {
                // display the coordinates as text
                return "$" + d.data;
            })
            .attr("x", function(d, i) {
                // center text with middle alignment
                return _self.xScale(i) + _self.xScale.rangeBand() / 2;
            })
            .attr("y", function(d) {
                // offset data inside the bar
                return _self.h - _self.padding;
            })
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("font-size", "11px")
            .attr("fill", "white");

        _self.textData
            .transition()
            .duration(1000)
            .attr("y", function(d, i) {
                // scale our bar height
                return _self.yScale(d.data)  + _self.padding + 11;
            })

        // create a group element, then create the title text
        _self.chartTitle = _self.svg.append("text")
            .text(function() {
                return _self.title + ' - South Lake Tahoe';
            })
            .attr("x", _self.w / 2)
            .attr("y", 24)
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("font-size", "24px")
            .attr("fill", "black");

    }

}