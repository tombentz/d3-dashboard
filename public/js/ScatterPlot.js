/**
 * This is a basic scatterplot chart with dynamic sizing, coloring, text and axis
 * Copyright © 2015 Thomas Bentz
 * Email: digitalblur1@gmail.com
 */
function BasicScatterPlot(viz_id, title, data_file){
    var _self = this;
    _self.padding = 30;

    _self.title = title;
    _self.viz_id = viz_id;
    _self.dataSource = data_file;

    this.init = function(barchart) {
//console.log(1, barchart);

        _self.w = d3.select(_self.viz_id).attr("width");
        _self.h = d3.select(_self.viz_id).attr("height");

        // load the data from external souce
        d3.json(_self.dataSource, function (error, json) {
            if(error) {
                return console.warn(error);
            }else{
                // add the bar chart to trigger a refresh on
                _self.createViz(json, barchart);
            }
        });
        return this;
    }


    this.refresh = function(state){
        var random_data = _self.getRandomData(state);
        this.updateViz(random_data);
    }

    this.updateViz = function(dataset){
        _self.circles
            .data(dataset)
            .transition()
            .duration(1000)
            // position x
            .attr("cx", function(d){
                return _self.xScale(d['data'][0]);
            })
            // position y
            .attr("cy", function(d){
                return _self.yScale(d['data'][1]);
            })
            // radius size based on y coordinate
            .attr("r", function(d) {
                return _self.rScale(d['data'][1]);
            })
            // dynamic color based on y coordinate
            .attr("fill", function(d) {
                return "rgb(236,9,40)";
            })

        _self.text
             .data(dataset)
             .transition()
             .duration(1000)
             .attr("x", function(d) {
                return _self.xScale(d['data'][0]);
             })
             .attr("y", function(d) {
                return _self.yScale(d['data'][1]+3);
             })
            .text(function(d, i){
              if(d['text']){
               return d['text'][i];
              }
              console.log(d['text'])
              console.log('here==', d, i);
//                return d['text'][i];
            })
    }

    this.getRandom = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    this.createViz = function(dataset, barchart){
        // setup our x scale
        _self.xScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d['data'][0]; })])
            .range([_self.padding, _self.w - _self.padding * 2]);

        // setup our y scale
        _self.yScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d['data'][1]; })])
            .range([_self.h - _self.padding, _self.padding]);

        // setup our circle radius/size scale based on y coordinate
        _self.rScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d['data'][1]; })])
            .range([4, 8]);

        // setup our circle color scale based on y coordinate
        _self.cScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d['data'][1]; })])
            .range([100, 255]);

        // setup x axis on the bottom of the viz
        _self.xAxis = d3.svg.axis()
                          .scale(_self.xScale)
                          .orient("bottom")
                          .ticks(5);

        _self.yAxis = d3.svg.axis()
                          .scale(_self.yScale)
                          .orient("left")
                          .ticks(5);

        // setup the svg canvas
        _self.svg = d3.select(_self.viz_id + '_group')
            .append("svg")
            .attr("width", _self.w)
            .attr("height", _self.h)
            .attr("id", "viz" + new Date().getMilliseconds());

        // get the position of the module location
        _self.svgx = d3.select(_self.viz_id).attr("x");
        _self.svgy = d3.select(_self.viz_id).attr("y");

        // set the position of the chart
        _self.svg.attr('x', _self.svgx);
        _self.svg.attr('y', _self.svgy);

        // create scatterplot from data
        _self.circles = _self.svg.selectAll("circle")
            .data(dataset)
            .enter()
            .append("circle")
            .attr("class", "top_ten_cities")
            .attr("id", function(d, i){
                return "item_" + i;
            })
            // position x
            .attr("cx", function(d){
                return _self.xScale(d['data'][0]);
            })
            // position y
            .attr("cy", function(d){
                return _self.yScale(d['data'][1]);
            })
            // radius size based on y coordinate
            .attr("r", function(d) {
                return _self.rScale(d['data'][1]);
            })

            // dynamic color based on y coordinate
            .attr("fill", function(d) {
                // get the color value from the scale based on the y coordinate
//                var g = Math.round(_self.cScale(d['data'][1]));
                return "rgb(236,9,40)";
//                return "rgb(70,70," + g + ")";
//                return "rgb(0, " + g + ", 0)";
            }).on("click", function (d, i) {
                console.log(barchart);
                var text = _self.svg.select("#text_" + i).text();
                barchart.refresh(text);
            })

        // create the text based on the data
        _self.text = _self.svg.selectAll("text")
            .data(dataset)
            .enter()
            .append("text")
            .text(function(d) {
                // display the coordinates as text
                return d['text'];
            })
            .attr("x", function(d) {
                return _self.xScale(d['data'][0]-20);
            })
            .attr("y", function(d) {
                return _self.yScale(d['data'][1])-13;
            })
            .attr("font-family", "sans-serif")
            .attr("font-size", "11px")
            .attr("fill", "rgb(72,79,85)")
            .attr("id", function(d, i){
                return "text_" + i;
            });

        // create the x axis last so it will display on top of everything else
        // we "translate" the scale to the bottom of the viz
        _self.svg.append("g")
            .attr('class', 'axis')
            .attr("transform", "translate(0," + (_self.h - _self.padding) + ")")
            .call(_self.xAxis);

        _self.svg.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(" + _self.padding + ",0)")
            .call(_self.yAxis);

    }


    this.getRandomData = function(state){
//        console.log(state);
        var states;
        switch(state){
            case "CA":
                states = [{0:"San Francisco"},
                          {1:"Los Angeles"},
                          {2:"Anaheim"},
                          {3:"San Diego"},
                          {4:"Venice Beach"},
                          {5:"Santa Monica"},
                          {6:"Santa Cruz"},
                          {7:"Napa"},
                          {8:"Monterey"},
                          {9:"South Lake Tahoe"}];
                break;
            case "TX":
                states = [{0:"Dallas"},
                          {1:"El Paso"},
                          {2:"Fredericksburg"},
                          {3:"Houston"},
                          {4:"Galveston"},
                          {5:"Kerrville"},
                          {6:"Marfa"},
                          {7:"San Antonio"},
                          {8:"South Padre Island"},
                          {9:"Austin"}];
                break;
            default:
                states = '';
        }

//        console.log('states txt', states, states[0]);

        var data = new Array();
        for(var i=0;i<=9;i++){
            var rand1 = this.getRandom(5, 480);
            var rand2 = this.getRandom(5, 100);
            data[i] = {"text": states[i], "data": [rand1, rand2]};
        }
//        console.log(data);
        return data;
    }

}