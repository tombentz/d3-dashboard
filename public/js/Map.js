/**
 * d3 map loading
 * Copyright © 2015 Thomas Bentz
 * Email: digitalblur1@gmail.com
 */


function Map(viz_id, title, svg_file){
    var _self = this;
    _self.padding = 20;

    _self.title = title;
    _self.viz_id = viz_id;
    _self.dataSource = svg_file;

    this.init = function(vis){
        _self.w = d3.select(_self.viz_id).attr("width");
        _self.h = d3.select(_self.viz_id).attr("height");
        // get the position of the module location
        _self.svgx = d3.select(_self.viz_id).attr("x");
        _self.svgy = d3.select(_self.viz_id).attr("y");

        var id = new Date().getMilliseconds();
        // setup the svg canvas
        _self.svg = d3.select(_self.viz_id + '_group')
            .append("svg")
            .attr("width", _self.w)
            .attr("height", _self.h)
            .attr("id", "viz_" + id)
            .attr('x', _self.svgx)
            .attr('y', _self.svgy)
            .append("g")
            .attr("width", _self.w)
            .attr("height", _self.h)
            .attr("id", "group_viz_" + id)
            .attr();

        var projection = d3.geo.albersUsa()
            .scale(700);

        var path = d3.geo.path()
            .projection(projection);

        d3.json(_self.dataSource, function(error, us) {
            _self.svg.selectAll(_self.viz_id)
                .data(topojson.feature(us, us.objects.states).features)
                .enter().append("path")
                .attr("d", path)
                .attr("class", "states")
                .attr("id", function(d){
                    return d.properties.postal;
//                    console.log(d.properties.postal);
                })
                .on("click", function(d){
                  if(d.properties.postal == 'CA' || d.properties.postal == 'TX'){
//                     console.log(vis, d)
                     vis.refresh(d.properties.postal);
                  }

                });
        });

        d3.select("#group_viz_" + id)
            .attr("transform", "scale(" + _self.w/550 + ") translate(-200,0)")


    }

    this.createViz = function(){

    }
}

//var projection = d3.geo.mercator()
//    .scale(500)
//    .translate([width / 2, height / 2]);